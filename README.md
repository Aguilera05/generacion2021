## Introducción

Este es un ejercicio colaborativo, en el que utilizarás por primera vez una plataforma de control de versiones con Git.

El objetivo es crear un libro en LaTeX que contenga un capítulo escrito por cada uno de los estudiantes de Ciencias de Computación de la generación 2021. En este capítulo puedes escribir una breve presentación de ti mismo, incluyendo alguna ilustración que puede ser una foto de ti o algo que consideres que te representa. Esperamos que este ejercicio ayude a que la generación 2021 se conozca e identifique más fácilmente durante este periodo de clases a distancia. Si no sabes qué escribir en tu capítulo, aquí hay algunas ideas (solo escribe la información con la que te sientas cómodo/a de compartir):

- Nombre y edad
- De dónde eres
- Por qué elegiste estudiar Ciencias de la Computación
- Qué te gusta hacer en tu tiempo libre
- Alguna afición
- Acerca de tus mascotas

## Instrucciones

1. Ve al repositorio del proyecto [coolaborativo](https://gitlab.com/CComputacion/generacion2021) y da clic en "Request Access".

    ![https://s3.us-west-2.amazonaws.com/secure.notion-static.com/031f1af5-7336-4817-afa1-2978f26d5034/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212347Z&X-Amz-Expires=86400&X-Amz-Signature=a8d10259de43706562efdd5dbd1df7f7d8c18d34d12aa2102fa91ebad5eb3e98&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/031f1af5-7336-4817-afa1-2978f26d5034/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212347Z&X-Amz-Expires=86400&X-Amz-Signature=a8d10259de43706562efdd5dbd1df7f7d8c18d34d12aa2102fa91ebad5eb3e98&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22)

2. Ahora da clic en el botón Clone y copia la URL **HTTPS** para clonar el repositorio desde la terminal.

    ![https://s3.us-west-2.amazonaws.com/secure.notion-static.com/ede89404-b26e-4754-86ef-1dae55b18708/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212149Z&X-Amz-Expires=86400&X-Amz-Signature=8b298330a08fe6f25b8fabed80746cb6378f7f2af55106b3ebab329fbabcdba7&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/ede89404-b26e-4754-86ef-1dae55b18708/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212149Z&X-Amz-Expires=86400&X-Amz-Signature=8b298330a08fe6f25b8fabed80746cb6378f7f2af55106b3ebab329fbabcdba7&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22)

3. Ve a la terminal y ejecuta el comando `git clone` seguido de la URL que acabas de copiar.

    `git clone [https://gitlab.com/CComputacion/generacion2021.git](https://gitlab.com/CComputacion/generacion2021.git)`

4. Crea y pasa a trabajar a una rama que tenga como nombre tu número de cuenta.

    `git branch [no. cuenta]`
    `git checkout [no. cuenta]`

5. Abre tu editor de código y edita el archivo `tex` que se encuentra dentro de la carpeta que tiene como nombre tu número de cuenta. También puedes agregar las ilustraciones que necesites para tu texto, solo recuerda mantenerlas dentro de la carpeta correspondiente a tu capítulo.
6. Cuando hayas terminado tus cambios, guárdalos y realiza un commit.

    `git add -A`

    `git commit -m "mi capítulo está listo"`

7. Ahora has push al repositorio en línea.

    `git push origin [no. cuenta]`

8. Ve a [`https://gitlab.com/CComputacion/generacion2021`](https://gitlab.com/CComputacion/generacion2021) y selecciona la rama que corresponde a tu capítulo.

    ![https://s3.us-west-2.amazonaws.com/secure.notion-static.com/b406a5c7-3cc5-479c-9f81-dc7fca582943/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212027Z&X-Amz-Expires=86400&X-Amz-Signature=36a9af8921825ed7e2152fa2da4004779f7edeeccfdfe55bf30f4aa038b34dfc&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/b406a5c7-3cc5-479c-9f81-dc7fca582943/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212027Z&X-Amz-Expires=86400&X-Amz-Signature=36a9af8921825ed7e2152fa2da4004779f7edeeccfdfe55bf30f4aa038b34dfc&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22)

9. Ahora da clic en el botón **Create Merge Request**

    ![https://s3.us-west-2.amazonaws.com/secure.notion-static.com/1cafda13-9ae7-41ac-b68e-3b93562ba2cf/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212229Z&X-Amz-Expires=86400&X-Amz-Signature=70a3c0a18525057bbb9e813efffcc7084bff92b41141a6e1baa1a3d813cd0215&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/1cafda13-9ae7-41ac-b68e-3b93562ba2cf/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212229Z&X-Amz-Expires=86400&X-Amz-Signature=70a3c0a18525057bbb9e813efffcc7084bff92b41141a6e1baa1a3d813cd0215&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22)

10. El título de tu solicitud de merge es, por omisión, el comentario de tu commit. Agregar descripción o comentarios es opcional. No es necesario que edites nada
11. Ve a la parte inferior de la página de creación de nueva solicitud de `merge` y da clic en **Submit merge request**

    ![https://s3.us-west-2.amazonaws.com/secure.notion-static.com/01e649ec-dc6a-428a-ab0e-0e26d299639b/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212455Z&X-Amz-Expires=86400&X-Amz-Signature=9a83732c1c0d7a1f1e70c87e8f5c8c36d58bf6df0e2c27b3bfdab53e1498172c&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22](https://s3.us-west-2.amazonaws.com/secure.notion-static.com/01e649ec-dc6a-428a-ab0e-0e26d299639b/Untitled.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201002%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201002T212455Z&X-Amz-Expires=86400&X-Amz-Signature=9a83732c1c0d7a1f1e70c87e8f5c8c36d58bf6df0e2c27b3bfdab53e1498172c&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Untitled.png%22)

12. Ahora espera a que los tutores del propedéutico acepten las solicitudes de merge. Te avisaremos por correo cuando el libro esté listo 🙂